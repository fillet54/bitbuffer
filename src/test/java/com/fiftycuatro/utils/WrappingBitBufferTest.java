package com.fiftycuatro.utils;

import org.junit.Test;

import static org.junit.Assert.*;

public class WrappingBitBufferTest {

    @Test
    public void positionIsInitializedToZero() {
        BitBuffer bb = new WrappingBitBuffer(new byte[0]);

        assertEquals(0, bb.getPosition());
    }

    @Test
    public void limitIsInitializedToWrappedArrayLength() {
        BitBuffer bb1 = new WrappingBitBuffer(new byte[10]);
        BitBuffer bb2 = new WrappingBitBuffer(new byte[23]);

        assertEquals(10, bb1.getLimit());
        assertEquals(23, bb2.getLimit());
    }

    @Test
    public void capacityIsInitializedToWrappedArrayLength() {
        BitBuffer bb1 = new WrappingBitBuffer(new byte[10]);
        BitBuffer bb2 = new WrappingBitBuffer(new byte[23]);

        assertEquals(10, bb1.getCapacity());
        assertEquals(23, bb2.getCapacity());
    }

    @Test
    public void addingMultipleBytes() {
        byte[] backing = new byte[10];
        BitBuffer bb = new WrappingBitBuffer(backing);

        bb.put((byte)0x0A);
        bb.put((byte)0x0B);
        bb.put((byte)0x0C);

        assertEquals((byte)0x0A, backing[0]);
        assertEquals((byte)0x0B, backing[1]);
        assertEquals((byte)0x0C, backing[2]);
    }


    @Test
    public void rewindSetsLimitToPositionAndPositionToZero() {
        BitBuffer bb = new WrappingBitBuffer(new byte[10]);
        bb.put((byte)0x0A);
        bb.put((byte)0x0B);
        bb.put((byte)0x0C);

        bb.rewind();

        assertEquals(0, bb.getPosition());
        assertEquals(8*3, bb.getLimit());
    }

    @Test
    public void getRetrievesFromPosition() {
        BitBuffer bb = new WrappingBitBuffer(new byte[10]);
        bb.put((byte)0x0A);
        bb.put((byte)0x0B);
        bb.put((byte)0x0C);

        bb.setPosition(8 * 1);

        assertEquals((byte)0x0B, bb.get());
    }

    @Test
    public void getIncrementsPositionByNumOfBitsInByte() {
        BitBuffer bb = new WrappingBitBuffer(new byte[10]);
        bb.put((byte)0x0A);

        bb.rewind();
        bb.get();

        assertEquals(8 * 1, bb.getPosition());
    }


}
