package com.fiftycuatro.utils;

public interface BitBuffer {
    void setPosition(int position);
    int getPosition();
    int getLimit();
    int getCapacity();

    void rewind();

    void put(byte b);
    byte get();
}
