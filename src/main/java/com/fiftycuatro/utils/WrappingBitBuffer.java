package com.fiftycuatro.utils;

public class WrappingBitBuffer implements BitBuffer {

    private static int BITS_IN_BYTE = 8;

    private final byte[] backing;
    private int position;
    private int limit;

    public WrappingBitBuffer(byte[] bytes) {
        this.backing = bytes;

        this.position = 0;
        this.limit = bytes.length;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    }

    public int getLimit() {
        return limit;
    }

    public int getCapacity() {
        return backing.length;
    }

    public void rewind() {
        this.limit = this.position;
        this.position = 0;
    }

    public void put(byte b) {
        backing[position / BITS_IN_BYTE] = b;
        position += BITS_IN_BYTE;
    }

    public byte get() {
        byte result = backing[position / BITS_IN_BYTE];
        position += BITS_IN_BYTE;
        return result;
    }

}
